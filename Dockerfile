# Build AdonisJS
FROM node:14-alpine as builder
# Workaround for now, since bodyparser install relies on Git
RUN apk add --no-cache git
# Set directory for all files
WORKDIR /home/node
# Copy over package.json files
COPY package*.json ./
# Install all packages
RUN npm install
# Copy over source code
COPY . .
# Workaround to make ace build working
ENV ENV_SILENT=true
ENV APP_KEY=temptemptemptemptemp
# Build AdonisJS for production
RUN node ace build --production

# Install packages on different step,
# since bodyparser install requires git
# but runtime does not need it
FROM node:14-alpine as installer
# Workaround
RUN apk add --no-cache git
# Set directory for all files
WORKDIR /home/node
# Copy over package.json files
COPY package*.json ./
# Install only prod packages
RUN npm ci --only=production

# Build final runtime container
FROM node:14-alpine
# Set environment variables
ENV NODE_ENV=production
# Disable .env file loading
ENV ENV_SILENT=true
ENV SESSION_DRIVER=cookie
ENV PORT=3333
ENV HOST=0.0.0.0
ENV CACHE_VIEWS=true
ENV APP_KEY=
ENV DB_PASSWORD=
ENV DB_HOST=
ENV RELEASE_TAG=
# Make directory for app to live in
RUN mkdir -p /home/node/app/
# Set working directory
WORKDIR /home/node/app
# Copy over required files from previous steps
# Copy over built files
COPY --from=builder /home/node/build ./build
# Copy over node_modules
COPY --from=installer /home/node/node_modules ./node_modules
# Copy over package.json files
COPY package*.json ./
# Set dir to Node
RUN chown -R node:node .
# Use non-root user
USER node
# Expose port 3333 to outside world
EXPOSE 3333
# Start server up
CMD [ "node", "./build/server.js" ]
