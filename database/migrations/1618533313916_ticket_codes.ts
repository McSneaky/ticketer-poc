import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TicketCodes extends BaseSchema {
  protected tableName = 'tickets'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('code')
      table.dateTime('valid_until')
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
