/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import Ticket from 'App/Models/Ticket'
import crypto from 'crypto'
import { DateTime } from 'luxon'

// Show the "welcome" page
Route.on('/').render('welcome')

// Simulates creating new ticket with some random code
// It's only used for testing, in live this data should come from Fienta API
Route.get('/create', async () => {
  // Create new ticket to DB
  let ticket = await Ticket.create({
    code: crypto.randomBytes(5).toString('hex'),
  })
  // Return only the code for easier copy-pasting
  return ticket.code
})

// Lists all tickets, just for debugging, should be removed in prod
// Or put behind some kind of admin password
Route.get('/list', async () => {
  return Ticket.all()
})

// "Activates" the ticket link and asks user to go to his unique link
Route.post('/register', async ({ request, response }) => {
  // Check if code already exists or not
  let ticket = await Ticket.findBy('code', request.input('code'))
  // If such code doesn't exist, say it's not existing
  if (!ticket) {
    return 'Ticket not found'
  }
  // Check if ticket has been used already
  if (ticket.validUntil) {
    return 'Ticket already used, can\t re-use same ticket again'
  }

  // Ticket haven't been used and it's existing. Set it's validUntil time
  // @ts-ignore
  ticket.validUntil = new DateTime({}).plus({ minutes: 1 })
  await ticket.save()

  // Make it return some HTML so link is clickable
  response.header('content-type', 'text/html')

  // Set cookie to ensure link can't be shared across browsers / devices
  response.cookie('ticket', ticket.code, {
    expires: ticket.validUntil.toJSDate(),
  })

  return `Ticket activated. Go to <a href="/ticket/${ticket.code}">/ticket/${ticket.code}</a> to view limited time content`
})

// Unique link per ticket
Route.get('/ticket/:id', async ({ params, response, request }) => {
  // Check if cookie is set. If there's no cookie then most likely tried to share link with friend
  if (request.cookie('ticket') !== params.id) {
    return response.forbidden('Ur not allowed to use that link')
  }
  // Search for ticket that's still unused and has been activated
  let ticket = await Ticket.query()
    .where({ code: params.id })
    .where('valid_until', '>', 'NOW()')
    .whereNotNull('valid_until')
    .first()

  // Such ticket was not found, meaning that it expired
  if (!ticket) {
    return 'Ticket expired or not found'
  }

  // Return expired date, just for testing. In prod it should show some page content
  return 'This page will expire at ' + new Date(ticket.validUntil)
})
